package com.harundemir918.yuztemeleserozet;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Layout;
import android.widget.TextView;

public class SummaryActivity extends AppCompatActivity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        textView = findViewById(R.id.textView);

        // Kitap ad ve özet değerleri alındı
        Intent intent = getIntent();
        String bookName = intent.getStringExtra("bookName");
        String bookSummary = intent.getStringExtra("bookSummary");

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(bookName);
        actionBar.setDisplayHomeAsUpEnabled(true);

        textView.setText(bookSummary);

        // TextView'deki yazının iki yana yaslı olması sağlandı
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            textView.setJustificationMode(Layout.JUSTIFICATION_MODE_INTER_WORD);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
